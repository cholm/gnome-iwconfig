// generated 2005/10/30 0:55:13 CEST by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to main_window.hh_new

// you might replace
//    class foo : public foo_glade { ... };
// by
//    typedef foo_glade foo;
// if you didn't make any modifications to the widget

#ifndef _MAIN_WINDOW_HH
#  include <main_window_glade.hh>
#  define _MAIN_WINDOW_HH
#  ifndef __LIST__
#    include <list>
#  endif
#  ifndef __MAP__
#    include <map>
#  endif
#  ifndef __STRING__
#    include <string>
#  endif
#  ifndef IWLIB_H
#    include <iwlib.h>
#  endif
#  ifndef  _GTKMM_LISTSTORE
#     include <gtkmm/liststore.h>
#  endif
#  ifndef _INTERFACE_DIALOG_HH
#    include <interface_dialog.hh>
#  endif 
#  ifndef _ESSID_INFO_H
#    include <essid_info.hh>
#  endif
#  ifndef _ESSID_DIALOG_HH
#    include <essid_dialog.hh>
#  endif 

class main_window : public main_window_glade
{ 
public:
  typedef Glib::ustring string_type;
  typedef std::list<string_type>           string_list;
  typedef std::map<string_type,essid_info> essid_list;

  void on_interface_combo_changed();
  void on_help_button_clicked();
  void on_selection_changed();
  void on_scan_button_clicked();  
  void on_cancel_button_clicked();
  void on_close_button_clicked();
  void on_config_button_clicked();
  void on_essid_button_clicked();
  void get_info(char* ifname);
  void update_interfaces();

  static main_window& instance();
private:
  main_window();
  static main_window* _instance;
  interface_dialog*   _ifdialog;
  essid_dialog*       _edialog;
  struct iwreq        _req;
  
  int                  _socket;
  string_list          _interfaces;
  string_type          _current;
  bool                 _init;
  essid_list           _essids;
  essid_list::iterator _essid;
  
  void scan();
  void error(const char* format, ...);
  void set_value(int request, const char* rname);
  bool get_value(int request, const char* rname);

  struct ap_columns : public Gtk::TreeModel::ColumnRecord
  {
    ap_columns() 
    {
      add(_st);
      add(_essid);
      add(_freq);
      add(_mode);
      add(_proto);
      add(_rate);
      add(_secu);
      add(_addr);
    }
    Gtk::TreeModelColumn<int>           _st;
    Gtk::TreeModelColumn<string_type>   _essid;
    Gtk::TreeModelColumn<string_type>   _freq;
    Gtk::TreeModelColumn<string_type>   _mode;
    Gtk::TreeModelColumn<string_type>   _proto;
    Gtk::TreeModelColumn<string_type>   _rate;
    Gtk::TreeModelColumn<string_type>   _secu;
    Gtk::TreeModelColumn<string_type>   _qual;
    Gtk::TreeModelColumn<string_type>   _addr;
  };
  
  ap_columns _ap_columns;
  Glib::RefPtr<Gtk::ListStore> _tree_model;
  Glib::RefPtr<Gtk::TreeSelection> _selection;
  
};
#endif
