// generated 2005/10/30 1:12:03 CEST by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to interface_dialog.hh_new

// you might replace
//    class foo : public foo_glade { ... };
// by
//    typedef foo_glade foo;
// if you didn't make any modifications to the widget

#ifndef _INTERFACE_DIALOG_HH
#  include "interface_dialog_glade.hh"
#  define _INTERFACE_DIALOG_HH
#  ifndef IWLIB_H
#    include <iwlib.h>
#  endif
#  ifndef  _GTKMM_LISTSTORE
#     include <gtkmm/liststore.h>
#  endif

class interface_dialog : public interface_dialog_glade
{  
public:        
  typedef Glib::ustring string_type;
  interface_dialog(int sock);
  void on_close_button_clicked();
  void on_apply_button_clicked() {}


  void on_nwid_on_toggled();
  void on_nwid_spin_changed();

  void on_mode_entry_changed();

  void on_freq_unit_entry_changed();
  void on_freq_spin_changed();
  void on_channel_spin_changed();
  void on_channel_on_toggled();

  void on_sens_unit_entry_changed();
  void on_sens_spin_changed();

  void on_nick_entry_activate();

  void on_rate_auto_toggle();
  void on_rate_spin_changed();

  void on_rts_on_toggled();
  void on_rts_auto_toggled();
  void on_rts_spin_changed();

  void on_frag_on_toggled();
  void on_frag_auto_toggled();
  void on_frag_spin_changed();

  void on_power_on_toggled();
  void on_power_mode_entry_changed();
  void on_power_period_spin_changed();
  void on_power_timeout_spin_changed();

  void on_retry_limit_spin_changed();
  void on_retry_lifetime_spin_changed();

  void on_txpower_on_toggled();
  void on_txpower_spin_changed();

  void on_enc_on_toggled();
  void on_enc_mode_changed();
  void on_selection_changed();

  void set_values(const string_type& name);
private:
  string_type     _name;
  wireless_info*  _info;
  int             _socket;
  iwreq           _req;
  bool            _init;
  int             _old_channel;
  
  void changed();
  void update_nwid();
  void update_mode();
  void update_freq();
  void update_sens();
  void update_nick();
  void update_rate();
  void update_rts();
  void set_rts_request();
  void update_frag();
  void set_frag_request();
  void update_power();
  void update_retry();
  void update_txpower();
  void set_txpower_request();
  void update_enc();
  
  void set_value(int req, const char* rname);
  bool get_value(int request, const char* rname);
  void error(const char* format, ...);

  struct key_columns : public Gtk::TreeModel::ColumnRecord
  {
  public:
    key_columns() { add(_number); add(_value); }
    Gtk::TreeModelColumn<int>         _number;
    Gtk::TreeModelColumn<string_type> _value;
  };
  key_columns _key_columns;
  Glib::RefPtr<Gtk::ListStore> _key_model;
  Glib::RefPtr<Gtk::TreeSelection> _key_selection;
};
#endif
