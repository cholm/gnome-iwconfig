// generated 2005/10/30 0:55:13 CEST by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to gnome-wlan-ap.cc_new

// This file is for your program, I won't touch it again!

#include <config.h>
#include "util.hh"
#include <gtkmm/main.h>
#include <glib/gi18n.h>
#include <stdexcept>
#include <iostream>
#include "main_window.hh"

bool debug_guard::_debug = false;

int main(int argc, char **argv)
{  
#if defined(ENABLE_NLS)
   bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
   bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
   textdomain (GETTEXT_PACKAGE);
#endif //ENABLE_NLS
   int ret = 0;
   try {
     Gtk::Main m(&argc, &argv);
     main_window& mw = main_window::instance();
     for (size_t i = 1; i < argc; i++) {
       if (argv[i][0] == '-') {
	 switch (argv[i][1]) {
	 case 'd': debug_guard::_debug = true; break;
	 default: break;
	 }
       }
     }
     mw.update_interfaces();
     m.run(mw);
   } catch (std::exception& e) {
     std::cerr << e.what() << std::endl;
     ret = 1;
   }
   // delete main_window;
   return ret;
}
