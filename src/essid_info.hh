#ifndef _ESSID_INFO
#  define _ESSID_INFO
#  ifndef __IOSTREAM__
#    include <iostream>
#  endif
#  ifndef _GLIBMM_USTRING_H
#    include <glibmm/ustring.h>
#  endif

struct essid_info
{
  typedef Glib::ustring string_type;
  string_type _addr;
  string_type _essid;
  int         _key_index;
  string_type _key_value;
  string_type _enc_mode;
  int         _nwid;
};

inline std::ostream& operator<<(std::ostream& s, const essid_info& i) 
{
  s << "MAC:  " << i._addr  << "\n"
    << "Name: " << i._essid << "\n" 
    << "Key:  " << i._key_value   << "\n"
    << "Mode: " << i._enc_mode;
  return s;
}
#endif
//
// EOF
//

