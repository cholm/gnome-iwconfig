// generated 2005/10/30 0:55:13 CEST by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to main_window.cc_new

// This file is for your program, I won't touch it again!
#include "main_window.hh"
#include "util.hh"
#include <stdexcept>
#include <iostream>
#include <gtkmm/messagedialog.h>
#include <gtkmm/liststore.h>
#include <sstream>
#include <gtkmm/cellrendererprogress.h>

main_window* main_window::_instance = 0;

//____________________________________________________________________
static int enum_handler(int sck, char* ifname, char**, int) 
{
  main_window& mw = main_window::instance();
  mw.get_info(ifname);
  return 0;
}

//____________________________________________________________________
main_window::main_window() 
  : _ifdialog(0), _edialog(0), _init(false)
{
  if ((_socket = iw_sockets_open()) < 0) {
    throw std::runtime_error(strerror(errno));
    return;
  }
  _tree_model = Gtk::ListStore::create(_ap_columns);
  _ap_view->set_model(_tree_model);

  Gtk::CellRendererProgress* cell = new Gtk::CellRendererProgress;
  int cols_count = _ap_view->append_column("Strength", *cell);
  Gtk::TreeViewColumn* pcolumn = _ap_view->get_column(cols_count - 1);
  if (pcolumn) 
    pcolumn->add_attribute(cell->property_value(), _ap_columns._st);
  _ap_view->append_column("ESSID", _ap_columns._essid);
  _ap_view->append_column("Freq./Channel", _ap_columns._freq);
  _ap_view->append_column("Mode", _ap_columns._mode);
  _ap_view->append_column("Protocol", _ap_columns._proto);
  _ap_view->append_column("Rate", _ap_columns._rate);
  _ap_view->append_column("Security", _ap_columns._secu);

  _selection = _ap_view->get_selection();
  _selection->signal_changed()
    .connect(sigc::mem_fun(*this, &main_window::on_selection_changed));
  _essid_button->set_sensitive(false);
  _essid = _essids.end();
  _init = true;
}

//____________________________________________________________________
main_window& main_window::instance() 
{
  if (!_instance) {
    _instance = new main_window;
  }
  return *_instance;
}

//____________________________________________________________________
void main_window::get_info(char* ifname)
{
  debug_guard g("get-info for %s", ifname);
  wireless_info info;
  if (iw_get_basic_config(_socket, ifname, &(info.b)) < 0) return;
  _interfaces.push_back(ifname);
}

//____________________________________________________________________
void main_window::update_interfaces()
{
  debug_guard g("update_interfaces");
  iw_enum_devices(_socket, &enum_handler, NULL, 0);
  _interface_combo->set_popdown_strings(_interfaces);
}



//____________________________________________________________________
void main_window::on_interface_combo_changed()
{
  debug_guard g("on_interface_combo_changed");
  Gtk::Entry* entry = _interface_combo->get_entry();
  if(entry) {
    string_type text = entry->get_text();
    //We seem to get 2 signals, one when the text is empty.
    if(text.empty()) return;
    g.message("New interface: %s", text.c_str());
    _current = text;
    scan();
  }
}

//____________________________________________________________________
void main_window::on_selection_changed()
{
  debug_guard g("on_selection_changed");
  if (!_init) return;
  Gtk::TreeModel::iterator iter = _selection->get_selected();
  if(iter) { //If anything is selected
    Gtk::TreeModel::Row row = *iter;
    //Do something with the row.
    _essid = _essids.find(row[_ap_columns._addr]);
    if (_essid == _essids.end()) {
      std::stringstream s;
      s << "Unknown access point: " << row[_ap_columns._addr] 
	<< " ESSID: " << row[_ap_columns._essid];
      throw std::runtime_error(s.str());
    }
    _essid_button->set_sensitive(true);
    char essid[IW_ESSID_MAX_SIZE + 1];
    strcpy(essid, _essid->second._essid.c_str());
    _req.u.essid.pointer = caddr_t(essid);
    _req.u.essid.length  = _essid->second._essid.size()+1;
    _req.u.essid.flags   = 1;
    g.message("Setting ESSID to %s", essid);
    set_value(SIOCSIWESSID, "ESSID");
    _req.u.essid.pointer  = caddr_t(NULL);
    _req.u.essid.length   = 0;
    _req.u.essid.flags    = 0;
    _req.u.data.pointer   = caddr_t(NULL);
    _req.u.data.length    = 0;
    _req.u.data.flags     = 0;
    if (_essid->second._key_index <= 0 && _essid->second._key_value.empty())
      return;
    g.message("Setting key for ESSID %s", essid);
    if (_essid->second._key_index > 0) {
      // Set the key by index 
      _req.u.encoding.flags |= _essid->second._key_index;
    }
    else if (!_essid->second._key_value.empty() && 
	     _essid->second._key_value != "on") {
      // Set key by value
      unsigned char key[IW_ENCODING_TOKEN_MAX];
      int len = iw_in_key_full(_socket, _current.c_str(), 
			       _essid->second._key_value.c_str(),
			       key, &_req.u.data.flags);
      if (len <= 0 && _essid->second._key_value != "off") {
	error("Invalid key: '%s' for interface %s and essid %s", 
	      _essid->second._key_value.c_str(), _current.c_str(), 
	      _essid->second._essid.c_str());
	return;
      }
      _req.u.data.length  = len;
      _req.u.data.pointer = caddr_t(key);
      const char* tmp = (const char*)key;
      string_type skey(tmp, size_t(len));
      g.message("Setting key to '%s' -> '%s'", 
		_essid->second._key_value.c_str(), skey.c_str());
    }
    if (_req.u.data.pointer == NULL) _req.u.data.flags |= IW_ENCODE_NOKEY;
    if (_essid->second._enc_mode =="open")      
      _req.u.data.flags |= IW_ENCODE_OPEN;
    if (_essid->second._enc_mode=="restricted") 
      _req.u.data.flags |= IW_ENCODE_RESTRICTED;
    if (_essid->second._enc_mode=="temporary")  
      _req.u.data.flags |= IW_ENCODE_TEMP;
    set_value(SIOCSIWENCODE, "Encode");
  }
}

//____________________________________________________________________
void main_window::on_scan_button_clicked()
{
  debug_guard g("on_scan_button_clicked");
  if (!_init) return;
  g.message("Rescanning");
  scan();
}


//____________________________________________________________________
void main_window::on_config_button_clicked()
{
  debug_guard g("on_config_button_clicked");
  if (!_ifdialog) {
    _ifdialog = new interface_dialog(_socket);
  }
  else 
    _ifdialog->show();
  _ifdialog->set_values(_current);
}

//____________________________________________________________________
void main_window::on_essid_button_clicked()
{
  debug_guard g("on_essid_button_clicked");
  if (_essid == _essids.end()) {
    error("No access point selected");
    return;
  }
  if (!_edialog) 
    _edialog = new essid_dialog(_socket);
  _edialog->set_values(_current, _essid->second);
  _edialog->run();
  on_selection_changed();
}

//____________________________________________________________________
void main_window::on_help_button_clicked()
{
  debug_guard g("on_help_button_clicked");
  std::cout << "No help yet" << std::endl;
}

//____________________________________________________________________
void main_window::on_cancel_button_clicked()
{
  debug_guard g("on_cancel_button_clicked");
  exit(0);
}

//____________________________________________________________________
void main_window::on_close_button_clicked()
{
  debug_guard g("on_close_button_clicked");
  exit(0);
}

//____________________________________________________________________
void main_window::scan()
{
  debug_guard g("scan");
  struct iw_range range;
  
  bool has_range= (iw_get_range_info(_socket, _current.c_str(), &range) >= 0);
  if (!has_range || (range.we_version_compiled < 14)) {
    error("Interface %-8.16.s does not support scanning",
	  _current.c_str());
    return;
  }
  // Initial timeout set to 250ms
  struct timeval  tv;
  tv.tv_sec    = 0;
  tv.tv_usec   = 250000;
  
  // Create a request.  
  struct iwreq    req;
  req.u.data.pointer = NULL;
  req.u.data.flags   = 0;
  req.u.data.length  = 0;
  
  if (iw_set_ext(_socket, _current.c_str(), SIOCSIWSCAN, &req) < 0) {
    if (errno != EPERM) {
      error("Interface %-8.16s does not support scanning:\n%s",
	    _current.c_str(), strerror(errno));
      return;
    }
    // Show left-over results
    std::cerr << "Showing left-over results" << std::endl;
    // Reset timeout
    tv.tv_usec = 0;
  }
  int timeout = 15000000 - tv.tv_usec;

  // Buffer 
  unsigned char* buffer = NULL;
  int            buflen = IW_SCAN_MAX_DATA;
  
  // Loop forever 
  while (true) {
    // Reset file descriptor set - must be done everr time 
    fd_set   fds;
    FD_ZERO(&fds);
    int last_fd = -1;
    
    // Wait until something happens 
    int ret = select(last_fd + 1, &fds, NULL, NULL, &tv);

    // Check for error 
    if (ret < 0) {
      // See if we should try again
      if (errno == EAGAIN || errno == EINTR) continue;
      error("Got a signal %s", strerror(errno));
      return;
    }

    // Check for timeout 
    if (ret == 0) {
      unsigned char* newbuf;
      
      while (true) {
	newbuf = static_cast<unsigned char*>(realloc(buffer, buflen));
	if (!newbuf) {
	  // Clean up
	  if (buffer) free(buffer);
	  throw std::runtime_error("out of memory");
	}
	buffer = newbuf;
	
	// Get results of a scan;
	req.u.data.pointer = buffer;
	req.u.data.flags   = 0;
	req.u.data.length  = buflen;
	if (iw_get_ext(_socket, _current.c_str(), SIOCGIWSCAN, &req) < 0) {
	  // Scan failed for some reason, check if it's because the
	  // buffer is too small 
	  if ((errno == E2BIG) && (range.we_version_compiled > 16)) {
	    buflen  = (req.u.data.length > buflen ? 
		       req.u.data.length : 2 * buflen);
	    continue;
	  }
	  // OK, so we may have to try again 
	  if (errno == EAGAIN) {
	    tv.tv_sec  =  0;
	    tv.tv_usec =  100000;
	    timeout    -=  tv.tv_usec;
	    if (timeout > 0) break;
	  }
	  // Otherwise we got some other (bad) error 
	  free(buffer);
	  error("Failed to read scan data from %-8.16s:\n%s", 
		_current.c_str(), strerror(errno));
	  return;
	}
	else 
	  break;
	if (errno != 0) continue;
	break;
      } // Realloc 
      break;
    }
  } // Top loop

  // Get the current ESSID 
  wireless_info info;
  string_type cur_essid;
  int         cur_index = 0;
  if(iw_get_basic_config(_socket, _current.c_str(), &(info.b)) >= 0) {
    if (info.b.has_essid && info.b.essid_on) {
      cur_essid = info.b.essid;
      cur_index = (info.b.essid_on & IW_ENCODE_INDEX);
      _essid_button->set_sensitive(true);
    }
    else 
      _essid_button->set_sensitive(false);
  }

  // Remove all the previous results 
  _tree_model->clear();
  
  if (req.u.data.length) {
    // Now parse the results 
    struct iw_event iwe;
    struct stream_descr stream;
    int    ap_num = 1;
    iw_init_event_stream(&stream, (char*)buffer, req.u.data.length);
    char buf[128];
    Gtk::TreeModel::Row row;
    essid_list::iterator ap;
    int ret;
    do {
      ret = iw_extract_event_stream(&stream, &iwe, 
					range.we_version_compiled);
      if (ret > 0) {
	// Get the fields 
	if (iwe.cmd == SIOCGIWAP) {
	  row = *(_tree_model->append());
	  string_type addr = iw_saether_ntop(&iwe.u.ap_addr, buf);
	  row[_ap_columns._addr] = addr;
	  ap = _essids.find(addr);
	  if (ap == _essids.end()) {
	    _essids[addr]._addr = addr;
	    ap = _essids.find(addr);
	  }
	  continue;
	}
	// (&iwe, ap_num, &range, has_range);
	switch (iwe.cmd) {
	case SIOCGIWNWID: 
	  // NWID 
	  ap->second._nwid = (iwe.u.nwid.disabled) ? -1 : iwe.u.nwid.value;
	  break;
	case SIOCGIWFREQ: {
	  // Frequency channel 
	  double freq    = iw_freq2float(&(iwe.u.freq));
	  int    channel = (has_range) ? iw_freq_to_channel(freq, &range) : -1;
	  iw_print_freq(buf, sizeof(buf),freq, channel, iwe.u.freq.flags);
	  row[_ap_columns._freq] = buf;
	  break;
	}
	case SIOCGIWMODE: 
	  row[_ap_columns._mode] = iw_operation_mode[iwe.u.mode];
	  break;
	case SIOCGIWNAME: 
	  row[_ap_columns._proto] = iwe.u.name;
	  break;
	case SIOCGIWESSID: {
	  if (iwe.u.essid.pointer && iwe.u.essid.length) {
	    memcpy(buf, iwe.u.essid.pointer, iwe.u.essid.length);
	    buf[iwe.u.essid.length] = '\0';
	  }
	  else {
	    buf[0] = 'a'; buf[1] = 'n'; buf[2] = 'y'; buf[3] = '\0';
	  }
	  row[_ap_columns._essid] = buf;
	  ap->second._key_index = (iwe.u.essid.flags & IW_ENCODE_INDEX);
	  if (row[_ap_columns._essid] == cur_essid) {
	    _init = false;
	    _essid = ap;
	    _selection->select(row);
	    _init = true;
	  }
	  ap->second._essid = row[_ap_columns._essid];
	  break;
	}
	case SIOCGIWENCODE: {
	  unsigned char	key[IW_ENCODING_TOKEN_MAX];
	  string_type   skey;
	  int           idx;
	  if (iwe.u.data.pointer)
	    memcpy(key, iwe.u.essid.pointer, iwe.u.data.length);
	  else
	    iwe.u.data.flags |= IW_ENCODE_NOKEY;
	  row[_ap_columns._secu] = "off";
	  if(iwe.u.data.flags & IW_ENCODE_DISABLED) skey = "off";
	  else {
	    // Display the key 
	    iw_print_key(buf,sizeof(buf),key,iwe.u.data.length,
			 iwe.u.data.flags);
	    skey = buf;
	    // Other information 
	    idx = (iwe.u.data.flags & IW_ENCODE_INDEX);
	    if(iwe.u.data.flags & IW_ENCODE_RESTRICTED)
	      row[_ap_columns._secu] = "restricted";
	    if(iwe.u.data.flags & IW_ENCODE_OPEN)
	      row[_ap_columns._secu] = "open";
	  }
	  ap->second._enc_mode  = row[_ap_columns._secu];
	  ap->second._key_value = skey;
	  ap->second._key_index = idx;
	  break;
	}
	case SIOCGIWRATE:
	  iw_print_bitrate(buf, sizeof(buf), iwe.u.bitrate.value);
	  row[_ap_columns._rate] = buf;
	  break;
	case IWEVQUAL:
	  int q = 0;
	  int d = 100;
	  if (has_range) {
	    if (!(iwe.u.qual.updated & IW_QUAL_QUAL_INVALID)) {
	      q = int(iwe.u.qual.qual);
	      d = int(range.max_qual.qual);
	    }
	    else if (!(iwe.u.qual.updated & IW_QUAL_LEVEL_INVALID) && 
		(iwe.u.qual.updated & IW_QUAL_DBM || 
		 (iwe.u.qual.level > range.max_qual.level))) {
	      q = int(iwe.u.qual.level - 0x100);
	      d = int(range.max_qual.level - 0x100);
	    }
	    else if (!(iwe.u.qual.updated & IW_QUAL_LEVEL_INVALID)) {
	      q = int(iwe.u.qual.level);
	      d = int(range.max_qual.level);
	    }
	  }
	  row[_ap_columns._st] = 100 * float(q) / d;
	  iw_print_stats(buf, sizeof(buf), &iwe.u.qual, &range, has_range);
	  // row[_ap_columns._qual] = buf;
	  break;
	}
      }
    } while (ret > 0);
  }
  free(buffer);
}

//____________________________________________________________________
void main_window::error(const char* format, ...)
{
  debug_guard g("error");
  va_list ap;
  va_start(ap, format);
  static char buffer[1024];
  vsnprintf(buffer, 1024, format, ap);
  va_end(ap);
  std::cerr << "Error: " << buffer << std::endl;
  Gtk::MessageDialog dia(buffer, false, Gtk::MESSAGE_ERROR);
  dia.run();
}

//____________________________________________________________________
void main_window::set_value(int request, const char* rname)
{
  debug_guard g("set_value(%X, %s)", request, rname);
  g.message("request is %s (%d) %s", (char*)_req.u.essid.pointer, 
	    _req.u.essid.length, (char*)_req.u.essid.pointer);
  if (iw_set_ext(_socket, _current.c_str(), request, &_req) < 0) 
    error("Error for wireless request \"SET %s\" on device %-1.16s:\n%s", 
	  rname, _current.c_str(), strerror(errno));
}


//____________________________________________________________________
bool main_window::get_value(int request, const char* rname)
{
  debug_guard g("get_value(%X, %s", request, rname);
  bool ret = (iw_get_ext(_socket, _current.c_str(), request, &_req) >= 0);
  if (!ret) std::cerr << '"' << rname << "\" " << std::hex 
		      << request << " on " << _current << ": " 
		      << strerror(errno) << std::endl;
  return ret;
}

//____________________________________________________________________
//
// EOF
//
