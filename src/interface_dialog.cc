// generated 2005/10/30 1:12:03 CEST by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to interface_dialog.cc_new

// This file is for your program, I won't touch it again!

#include "config.h"
#include "interface_dialog.hh"
#include "util.hh"
#include <iomanip>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <cstdarg>
#include <gtkmm/messagedialog.h>
#include <gtkmm/liststore.h>

//____________________________________________________________________
interface_dialog::interface_dialog(int sock) 
  : _socket(sock)
{
  _key_model = Gtk::ListStore::create(_key_columns);
  _key_view->set_model(_key_model);
  _key_view->append_column("#", _key_columns._number);
  _key_view->append_column_editable("Key", _key_columns._value);
  _key_selection = _key_view->get_selection();
  _key_selection->signal_changed()
    .connect(sigc::mem_fun(*this, &interface_dialog::on_selection_changed));
}

//____________________________________________________________________
void interface_dialog::on_selection_changed()
{}

//____________________________________________________________________
void interface_dialog::on_close_button_clicked()
{  
  this->hide();
}

//====================================================================
void interface_dialog::update_nwid()
{
  debug_guard g("interface_dialog::update_nwid");
  if (_info->b.has_nwid) _nwid_spin->set_value(_info->b.nwid.value);
  bool disabled = int(_info->b.nwid.disabled) != 0;
  _nwid_on->set_active(!disabled);
  _nwid_spin->set_sensitive(!disabled);
}

//____________________________________________________________________
void interface_dialog::on_nwid_spin_changed()
{
  debug_guard g("interface_dialog::on_nwid_entry_editing_done");
  if (!_init) return;
  if (!_nwid_on->get_active()) 
    _req.u.nwid.disabled = 1;
  else {
    _req.u.nwid.disabled = 0;
    int val = _nwid_spin->get_value_as_int();
    if (val != 0) _req.u.nwid.value = val;
  }
  _req.u.nwid.fixed = 1;
  set_value(SIOCSIWNWID, "NWID");
}

//____________________________________________________________________
void interface_dialog::on_nwid_on_toggled()
{
  debug_guard g("interface_dialog::on_nwid_on_toggled");
  if (!_init) return;
  bool on = _nwid_on->get_active();
  _nwid_spin->set_sensitive(on);
  changed();
}

//====================================================================
void interface_dialog::update_mode()
{
  debug_guard g("interface_dialog::update_mode");
  if (_info->b.has_mode) 
    _mode_list->get_entry()->set_text(iw_operation_mode[_info->b.mode]);  
}

//____________________________________________________________________
void interface_dialog::on_mode_entry_changed()
{
  debug_guard g("interface_dialog::on_mode_entry_changed");
  if (!_init) return;
  string_type text = _mode_list->get_entry()->get_text();
  g.message("new mode: %s", text.c_str());
  if (text.empty()) return;
  int k = 0;
  while((k < IW_NUM_OPER_MODE) && text != iw_operation_mode[k]) k++;
  if ((k >= IW_NUM_OPER_MODE) || (k < 0)) {
    error("Unknown mode: %s", text.c_str());
    return;
  }
  _req.u.mode = k;
  set_value(SIOCSIWMODE, "Mode");
}



//====================================================================
void interface_dialog::update_freq()
{
  debug_guard g("interface_dialog::update_freq");
  if(!_info->b.has_freq || _info->range.num_frequency <= 0) {
    _freq_spin->set_sensitive(false);
    return;
  }
  
  bool freq_fixed = (_info->b.freq_flags & IW_FREQ_FIXED);
  std::list<string_type> freqs;
  double cur_freq;
  int   cur_channel;
  cur_freq    = _info->b.freq;    // Frequency/channel 
  cur_channel = -1;              // Converted to channel 
  // Some drivers insist of returning channel instead of frequency. 
  // This fixes them up. Note that, driver should still return
  // frequency, because other tools depend on it. 
  if(_info->has_range && (cur_freq < KILO)) 
    cur_channel = iw_channel_to_freq((int) cur_freq, &cur_freq, &_info->range);
  
  char   buf[128];
  freqs.push_back("auto");
  for(int i = 0; i < _info->range.num_frequency; i++) {
    double freq    = iw_freq2float(&(_info->range.freq[i]));
    int    channel = _info->range.freq[i].i;
    iw_print_freq_value(buf, 128, freq);
    std::stringstream s;
    s << buf << " / Channel " << channel;
    freqs.push_back(s.str());
    if (freq == cur_freq) _freq_spin->get_entry()->set_text(s.str());
  }
  _freq_spin->set_popdown_strings(freqs);
  if (!freq_fixed) 
    _freq_spin->get_entry()->set_text("auto");
}

//____________________________________________________________________
void interface_dialog::on_freq_spin_changed()
{
  debug_guard g("interface_dialog::on_freq_spin_changed");
  if (!_init) return;
  string_type fval = _freq_spin->get_entry()->get_text();
  if (fval.empty()) return;
  double      val;
  if (fval == "auto") val = 0;
  else {
    string_type unit;
    std::stringstream sf(fval);
    sf >> val >> unit;
    if (unit == "GHz")   val *= GIGA;
    if (unit == "MHz")   val *= MEGA;
    if (unit == "kHz")   val *= KILO;
    g.message("Setting frequency to %f %s", val, unit.c_str());
  }
  if (val == 0) {
    _req.u.freq.m       = -1;
    _req.u.freq.e      = 0;
    _req.u.freq.flags  = 0;
  }
  else {
    iw_float2freq(val, &(_req.u.freq));
    _req.u.freq.flags = IW_FREQ_FIXED;
  }
  set_value(SIOCSIWFREQ, "Frequency");
}

//====================================================================
void interface_dialog::update_sens()
{
  debug_guard g("interface_dialog::update_sens");
  if(get_value(SIOCGIWSENS, "Get Sensitivity")) {
    _info->has_sens = 1;
    if(_info->sens.fixed && _info->has_range)
      /* Display in dBm ? */
      if(_info->sens.value < 0) {
	_sens_unit->set_text("dBm");
	_sens_spin->set_value(_info->sens.value);
      }
      else {
	_sens_unit->set_text("%");
	_sens_spin->set_value(_info->sens.value*100./_info->range.sensitivity);
	_sens_spin->set_range(0, _info->range.sensitivity);
      }
    else {
      _sens_unit->set_text("%");
      _sens_spin->set_value(_info->sens.value);
    }
  }
  else {
    _sens_spin->set_sensitive(false);
    _sens_unit->set_sensitive(false);
    _sens_label->set_sensitive(false);
  }
}

//____________________________________________________________________
void interface_dialog::on_sens_unit_entry_changed()
{
  debug_guard g("interface_dialog::on_sens_unit_entry_changed");
  if (!_init) return;
  double val = _sens_spin->get_value();
  if (val > 0)   val *= 100./_info->range.sensitivity;
  _sens_spin->set_value(val);
}

//____________________________________________________________________
void interface_dialog::on_sens_spin_changed()
{
  debug_guard g("interface_dialog::on_sens_spin_changed");
  if (!_init) return;
  int val = _sens_spin->get_value_as_int();
  _sens_unit->set_text(val < 0 ? "dBm" : "%");
  if (val > 0) val *= _info->range.sensitivity / 100;
  _req.u.sens.value = val;
  set_value(SIOCSIWSENS, "Sensitivity");
}

//====================================================================
void interface_dialog::update_nick()
{
  debug_guard g("interface_dialog::update_nick");
  // Get NickName 
  _req.u.essid.pointer = caddr_t(_info->nickname);
  _req.u.essid.length  = IW_ESSID_MAX_SIZE + 1;
  _req.u.essid.flags   = 0;
  _nick_entry->set_max_length(IW_ESSID_MAX_SIZE-1);
  if(get_value(SIOCGIWNICKN, "Get Nickname")) {
    if(_req.u.data.length > 1) {
      _info->has_nickname = 1;
      _nick_entry->set_text(_info->nickname);
    }
  }
}

//____________________________________________________________________
void interface_dialog::on_nick_entry_activate()
{
  debug_guard g("interface_dialog::on_nick_entry_activate");
  if (!_init) return;
  string_type text = _nick_entry->get_text();
  if (text.empty()) return;
  if (text.size() > IW_ESSID_MAX_SIZE) {
    error("Nick name \"%s\" too long: %d > %d", 
	  text.c_str(), text.size(), IW_ESSID_MAX_SIZE);
    return;
  }
  static char buf[IW_ESSID_MAX_SIZE];
  strcpy(buf, text.c_str());
  _req.u.essid.pointer = caddr_t(buf);
  _req.u.essid.length  = text.size()+1;
  set_value(SIOCSIWNICKN, "Nickname");
}

//====================================================================
void interface_dialog::update_rate()
{
  debug_guard g("interface_dialog::update_rate");
  // Display the currently used/set bit-rate 
  // Get bit rate 
  if(get_value(SIOCGIWRATE, "Get bitrate")) {
    _info->has_bitrate = 1;
    memcpy(&(_info->bitrate), &(_req.u.bitrate), sizeof(iwparam));
  }
  if(!_info->has_bitrate || _info->range.num_bitrates <= 0) {
    _rate_auto->set_sensitive(false);
    _rate_spin->set_sensitive(false);
  }

  char buf[128];
  std::list<string_type> rates;
  rates.push_back("auto");
  for(int k = 0; k < _info->range.num_bitrates; k++) {
    iw_print_bitrate(buf, 128, _info->range.bitrate[k]);
    /* Maybe this should be %10s */
    rates.push_back(buf);
  }
  _rate_spin->set_popdown_strings(rates);
  bool   disabled = int(_info->bitrate.disabled) != 0;
  bool   fixed    = int(_info->bitrate.fixed) != 0;
  double val      = _info->bitrate.value;
  if (!fixed) {
    iw_print_bitrate(buf, 128, int(val));
    _rate_spin->get_entry()->set_text(buf);
    _rate_auto->set_sensitive(true);
  }
  else {
    _rate_spin->get_entry()->set_text("auto");    
    _rate_auto->set_sensitive(false);
  }
}

//____________________________________________________________________
void interface_dialog::on_rate_auto_toggle()
{
  debug_guard g("interface_dialog::on_rate_auto_group_changed");
  if (!_init) return;
  double      val;
  string_type unit;
  string_type sval = _rate_spin->get_entry()->get_text();
  std::stringstream s(sval);
  s >> val >> unit;
  if (unit == "Gb/s")   val *= GIGA;
  if (unit == "Mb/s")   val *= MEGA;
  if (unit == "kb/s")   val *= KILO;
  _req.u.bitrate.value = long(val);
  if (_rate_auto->get_active()) _req.u.bitrate.fixed = 0;    
  else                          _req.u.bitrate.fixed = 1;    
  set_value(SIOCSIWRATE, "Bit Rate");
}

//____________________________________________________________________
void interface_dialog::on_rate_spin_changed()
{
  debug_guard g("interface_dialog::on_rate_spin_changed");
  if (!_init) return;
  string_type sval = _rate_spin->get_entry()->get_text();
  if (sval == "auto") {
    _req.u.bitrate.value = -1;
    _req.u.bitrate.fixed = 0;    
    _rate_auto->set_sensitive(false);
  }
  else {
    double val;
    string_type unit;
    std::stringstream s(sval);
    s >> val >> unit;
    if (unit == "Gb/s")   val *= GIGA;
    if (unit == "Mb/s")   val *= MEGA;
    if (unit == "kb/s")   val *= KILO;
    _rate_auto->set_sensitive(true);
    _req.u.bitrate.value = long(val);
    if (!_rate_auto->get_active())
      _req.u.bitrate.fixed = 1;
    else 
      _req.u.bitrate.fixed = 0;
  }
  set_value(SIOCSIWRATE, "Bit Rate");
}

//====================================================================
void interface_dialog::update_rts()
{
  debug_guard g("interface_dialog::update_rts");
  // Get RTS threshold 
  if (_info->has_range) 
    _rts_spin->set_range(_info->range.min_rts, _info->range.max_rts);
  if(get_value(SIOCGIWRTS, "Get RTS")) {
    _info->has_rts = 1;
    memcpy(&(_info->rts), &(_req.u.rts), sizeof(iwparam));
  }
  _rts_spin->set_value(_info->has_rts ? _info->rts.value : 0);
  _rts_spin->set_sensitive(!int(_info->rts.disabled));
  _rts_auto->set_sensitive(!int(_info->rts.disabled));
  _rts_on->set_active(!int(_info->rts.disabled));
  
}

//____________________________________________________________________
void interface_dialog::on_rts_on_toggled()
{
  debug_guard g("interface_dialog::on_rts_on_toggled");
  if (!_init) return;
  bool on = _rts_on->get_active();
  _rts_spin->set_sensitive(on);
  _rts_auto->set_sensitive(on);
  set_rts_request();
}

//____________________________________________________________________
void interface_dialog::on_rts_auto_toggled()
{
  debug_guard g("interface_dialog::on_rts_auto_group_changed");
  if (!_init) return;
  set_rts_request();
}

//____________________________________________________________________
void interface_dialog::on_rts_spin_changed()
{
  debug_guard g("interface_dialog::on_rts_spin_changed");
  if (!_init) return;
  set_rts_request();
}



//____________________________________________________________________
void interface_dialog::set_rts_request()
{
  _rts_spin->set_sensitive(!_rts_auto->get_active());
  _req.u.rts.value    = -1;
  _req.u.rts.fixed    =  1;
  _req.u.rts.disabled =  0;
  if (_rts_on->get_active()) {
    if (_rts_auto->get_active()) _req.u.rts.fixed = 0;
    else _req.u.rts.value = _rts_spin->get_value_as_int();
  }
  else 
    _req.u.rts.disabled = 1;
  set_value(SIOCSIWRTS, "RTS Threshold");
}

//====================================================================
void interface_dialog::update_frag()
{
  debug_guard g("interface_dialog::update_frag");
  // Get fragmentation threshold 
  if (_info->has_range) 
    _frag_spin->set_range(_info->range.min_frag, _info->range.max_frag);
  if(get_value(SIOCGIWFRAG, "Get fragmentation threshold")) {
    _info->has_frag = 1;
    memcpy(&(_info->frag), &(_req.u.frag), sizeof(iwparam));
    _frag_spin->set_value(_info->frag.value);
  }
  bool disabled = int(_info->frag.disabled) != 0;
  _frag_auto->set_active(!_info->frag.fixed);
  _frag_auto->set_sensitive(!disabled);
  _frag_spin->set_sensitive(!disabled && _info->frag.fixed);
}

//____________________________________________________________________
void interface_dialog::on_frag_on_toggled()
{
  debug_guard g("interface_dialog::on_frag_on_toggled");
  if (!_init) return;
  bool on = _frag_on->get_active();
  _frag_spin->set_sensitive(on);
  _frag_auto->set_sensitive(on);
  set_frag_request();
}

//____________________________________________________________________
void interface_dialog::on_frag_spin_changed()
{
  debug_guard g("interface_dialog::on_frag_spin_changed");
  if (!_init) return;
  set_frag_request();
}

//____________________________________________________________________
void interface_dialog::on_frag_auto_toggled()
{
  debug_guard g("interface_dialog::on_frag_auto_group_changed");
  if (!_init) return;
  set_frag_request();
}

//____________________________________________________________________
void interface_dialog::set_frag_request()
{
  _frag_spin->set_sensitive(!_frag_auto->get_active());
  _req.u.frag.value    = -1;
  _req.u.frag.fixed    =  1;
  _req.u.frag.disabled =  0;
  if (_frag_on->get_active()) {
    if (_frag_auto->get_active()) _req.u.frag.fixed = 0;
    else _req.u.frag.value = _frag_spin->get_value_as_int();
  }
  else 
    _req.u.frag.disabled = 1;
  set_value(SIOCSIWFRAG, "Fragmentation Threshold");
}

//====================================================================
void interface_dialog::update_power()
{
  debug_guard g("interface_dialog::update_power");
  if (_info->range.pm_capa & IW_POWER_MODE) {
    std::list<string_type> modes;
    if(_info->range.pm_capa & (IW_POWER_UNICAST_R | IW_POWER_MULTICAST_R))
      modes.push_back("All");
    if(_info->range.pm_capa & IW_POWER_UNICAST_R)
      modes.push_back("Unicast");
    if(_info->range.pm_capa & IW_POWER_MULTICAST_R)
      modes.push_back("Multicast");
    if(_info->range.pm_capa & IW_POWER_FORCE_S)
      modes.push_back("Force send");
    if(_info->range.pm_capa & IW_POWER_REPEATER)
      modes.push_back("Repeat multicast");
    _power_mode->set_popdown_strings(modes);
  }
  else 
    _power_mode->set_sensitive(false);
  if (_info->range.pmt_flags & IW_POWER_TIMEOUT) {
    _power_timeout_min_spin->set_range(_info->range.min_pmt,
				       _info->range.max_pmt);
    _power_timeout_max_spin->set_range(_info->range.min_pmt,
				       _info->range.max_pmt);
  }
  if (_info->range.pmp_flags & IW_POWER_PERIOD) {
    _power_period_min_spin->set_range(_info->range.min_pmp,
				       _info->range.max_pmp);
    _power_period_max_spin->set_range(_info->range.min_pmp,
				       _info->range.max_pmp);
  }
  

  _req.u.power.flags = 0;
  if (get_value(SIOCGIWPOWER, "Get power")) {
    _info->has_power = 1;
    memcpy(&(_info->power), &(_req.u.power), sizeof(iwparam));
    // Let's check the mode 
    switch (_info->power.flags & IW_POWER_MODE) {
    case IW_POWER_UNICAST_R: 
      _power_mode->get_entry()->set_text("Unicast");
      break;
    case IW_POWER_MULTICAST_R:
      _power_mode->get_entry()->set_text("Multicast");
      break;
    case IW_POWER_ALL_R:
      _power_mode->get_entry()->set_text("All");
      break;
    case IW_POWER_FORCE_S:
      _power_mode->get_entry()->set_text("Force send");
      break;
    case IW_POWER_REPEATER:
      _power_mode->get_entry()->set_text("Repeat multicast");
      break;
    } // switch

    if (_info->power.disabled) _power_on->set_active(false);
    for (int i = 0; i < 2; i++) {
      _req.u.power.flags = (i == 0 ? IW_POWER_TIMEOUT : IW_POWER_PERIOD);
      for (int j = 0; j < 2; j++) {
	Gtk::SpinButton* target =  0;
	_req.u.power.flags      |= (j == 0 ? IW_POWER_MIN : IW_POWER_MAX);
	switch (i * 2 + j) {
	case 0: target = _power_timeout_min_spin; break;
	case 1: target = _power_timeout_max_spin; break;
	case 2: target = _power_period_min_spin; break;
	case 3: target = _power_period_max_spin; break;
	}
	if (!get_value(SIOCGIWPOWER, "Get power limit/lifetime")) {
	  target->set_sensitive(false);
	  continue;
	}
	target->set_value(_req.u.power.value);
      }
    }
  }
  else {
    _power_on->set_sensitive(false);
    _power_mode->set_sensitive(false);
    _power_period_min_spin->set_sensitive(true);
    _power_timeout_min_spin->set_sensitive(true);
    _power_period_max_spin->set_sensitive(true);
    _power_timeout_max_spin->set_sensitive(true);
  }
}

//____________________________________________________________________
void interface_dialog::on_power_on_toggled()
{
  debug_guard g("interface_dialog::on_power_on_toggled");
  bool on = _power_on->get_active();
  _power_mode->set_sensitive(on);
  _power_period_min_spin->set_sensitive(on);
  _power_timeout_min_spin->set_sensitive(on);
  _power_period_max_spin->set_sensitive(on);
  _power_timeout_max_spin->set_sensitive(on);
}


//____________________________________________________________________
void interface_dialog::on_power_mode_entry_changed()
{
  debug_guard g("interface_dialog::on_power_mode_entry_changed");
  if (!_init) return;
  string_type text = _power_mode->get_entry()->get_text();
}

//____________________________________________________________________
void interface_dialog::on_power_period_spin_changed()
{
  debug_guard g("interface_dialog::on_power_period_spin_changed");
  if (!_init) return;
  double min = _power_period_min_spin->get_value();
  double max = _power_period_max_spin->get_value();
}

//____________________________________________________________________
void interface_dialog::on_power_timeout_spin_changed()
{
  debug_guard g("interface_dialog::on_power_timeout_spin_changed");
  if (!_init) return;
  double min = _power_timeout_min_spin->get_value();
  double max = _power_timeout_max_spin->get_value();
}


//====================================================================
void interface_dialog::update_retry()
{
  debug_guard g("interface_dialog::update_retry");
  // Get retry limit/lifetime 
  if ((_info->has_range) && 
      (_info->range.we_version_compiled > 10)) {
  debug_guard g("retry range: ");
    if (_info->range.retry_flags & IW_RETRY_LIMIT) {
      _retry_limit_min_spin->set_range(_info->range.min_retry,
				       _info->range.max_retry);
      _retry_limit_max_spin->set_range(_info->range.min_retry,
				       _info->range.max_retry);
    }
    if (_info->range.r_time_flags & IW_RETRY_LIFETIME) {
      _retry_lifetime_min_spin->set_range(_info->range.min_r_time,
					  _info->range.max_r_time);
      _retry_lifetime_max_spin->set_range(_info->range.min_r_time,
					  _info->range.max_r_time);
    }
  }
  _req.u.retry.flags = 0;
  for (int i = 0; i < 2; i++) {
    _req.u.retry.flags = (i == 0 ? IW_RETRY_LIFETIME : IW_RETRY_LIMIT);
    for (int j = 0; j < 2; j++) {
      Gtk::SpinButton* target =  0;
      _req.u.retry.flags      |= (j == 0 ? IW_RETRY_MIN : IW_RETRY_MAX);
      switch (i * 2 + j) {
      case 0: target = _retry_lifetime_min_spin; break;
      case 1: target = _retry_lifetime_max_spin; break;
      case 2: target = _retry_limit_min_spin; break;
      case 3: target = _retry_limit_max_spin; break;
      }
      if (!get_value(SIOCGIWRETRY, "Get retry limit/lifetime")) {
	target->set_sensitive(false);
	continue;
      }
      target->set_value(_req.u.retry.value);
      memcpy(&(_info->retry), &(_req.u.retry), sizeof(iwparam));
    }
  }
}

//____________________________________________________________________
void interface_dialog::on_retry_limit_spin_changed()
{
  debug_guard g("interface_dialog::on_retry_limit_spin_changed");
  if (!_init) return;
  if (!_retry_limit_min_spin->is_sensitive() || 
      !_retry_limit_max_spin->is_sensitive()) return;
  double min = _retry_limit_min_spin->get_value();
  double max = _retry_limit_max_spin->get_value();
}

//____________________________________________________________________
void interface_dialog::on_retry_lifetime_spin_changed()
{
  debug_guard g("interface_dialog::on_retry_lifetime_spin_changed");
  if (!_init) return;
  if (!_retry_lifetime_min_spin->is_sensitive() || 
      !_retry_lifetime_max_spin->is_sensitive()) return;
  double min = _retry_lifetime_min_spin->get_value();
  double max = _retry_lifetime_max_spin->get_value();
}

//====================================================================
void interface_dialog::update_txpower()
{
  debug_guard g("interface_dialog::update_txpower");
  // Get Transmit Power 
  if ((_info->has_range) && (_info->range.we_version_compiled > 9)) {
    if (_info->range.num_txpower <= 0) {
      std::cerr << "No txpower levels" << std::endl;
      _txpower_spin->set_sensitive(false);
    }
    if (_info->range.num_txpower > IW_MAX_TXPOWER) {
      std::cerr << "too many txpower levels " 
		<< _info->range.num_txpower << " > " 
		<< IW_MAX_TXPOWER << std::endl;
      _txpower_spin->set_sensitive(false);
    }
    std::list<string_type> powers;
    for(int k = 0; k < _info->range.num_txpower; k++) {
      std::stringstream s;
      /* Check for relative values */
      if(_info->range.txpower_capa & IW_TXPOW_RELATIVE) 
	s << _info->range.txpower[k] << " (a.u.)";
      else if (_info->range.txpower_capa & IW_TXPOW_MWATT) 
	s << iw_mwatt2dbm(_info->range.txpower[k]) << " dBm / " 
	  << _info->range.txpower[k] << " mW";
      else
	s << _info->range.txpower[k] << " dBm / " 
	  << iw_dbm2mwatt(_info->range.txpower[k]) << " mW";
      powers.push_back(s.str());
    }
    
    if (get_value(SIOCGIWTXPOW, "Get Transmission power")) {
      _info->has_txpower = 1;
      memcpy(&(_info->txpower), &(_req.u.txpower), sizeof(iwparam));
    
      int txpower = _info->txpower.value;
      std::stringstream s;
      if (_info->txpower.flags & IW_TXPOW_RELATIVE) 
	s << txpower << " (a.u.)";
      else if (_info->txpower.flags & IW_TXPOW_MWATT) 
	s << iw_mwatt2dbm(txpower) << " dBm / " << txpower << " mW";
      else 
	s << txpower << " dBm / " << iw_dbm2mwatt(txpower) << " mW";
      _txpower_spin->get_entry()->set_text(s.str());

      std::list<string_type>::const_iterator i = powers.begin();
      for (; i != powers.end(); ++i) if (*i == s.str()) break;
      if (i == powers.end()) powers.push_back(s.str());
    }
    bool disabled = int(_info->txpower.disabled) != 0;
    _txpower_on->set_active(!disabled);
    _txpower_spin->set_sensitive(!disabled);

    if (powers.size() != 1) powers.push_front("auto");
    else                    _txpower_on->set_sensitive(false);
    _txpower_spin->set_popdown_strings(powers);
  }
  else {
    _txpower_on->set_sensitive(false);
    _txpower_spin->set_sensitive(false);
  }  
}

//____________________________________________________________________
void interface_dialog::on_txpower_on_toggled()
{
  debug_guard g("interface_dialog::on_txpower_on_toggled");
  if (!_init) return;
  bool on = _txpower_on->get_active();
  _txpower_spin->set_sensitive(on);
  set_txpower_request();
}

//____________________________________________________________________
void interface_dialog::on_txpower_spin_changed()
{
  debug_guard g("interface_dialog::on_txpower_spin_changed");
  if (!_init) return;
  set_txpower_request();
}


//____________________________________________________________________
void interface_dialog::set_txpower_request()
{
  _req.u.txpower.value    = -1;
  _req.u.txpower.fixed    =  1;
  _req.u.txpower.disabled =  0;
  _req.u.txpower.flags    = IW_TXPOW_DBM;
  string_type text = _txpower_spin->get_entry()->get_text();
  if (_txpower_on->get_active()) {
    if (text == "auto") 
      _req.u.txpower.fixed = 0;
    else {
      string_type unit;
      int         val;
      std::stringstream s(text);
      s >> val >> unit;
      if (_info->range.txpower_capa & IW_TXPOW_RELATIVE) {
	if (unit == "mW") {
	  error("Cannot convert %fmW to relative strenght for Tx power", val);
	  return;
	}
      }
      else if (_info->range.txpower_capa & IW_TXPOW_MWATT) {
	if (unit == "dBm") val = iw_dbm2mwatt(val);
	_req.u.txpower.flags = IW_TXPOW_MWATT;
      }
      else {
	if (unit == "mW") val = iw_mwatt2dbm(val);
	_req.u.txpower.flags = IW_TXPOW_DBM;
      }
      _req.u.txpower.value = val;
    }
  }
  else 
    _req.u.txpower.disabled = 1;
  set_value(SIOCSIWTXPOW, "Transmission Power");
}

//====================================================================
void interface_dialog::update_enc()
{
  debug_guard g("interface_dialog::update_enc");
  if (_info->has_range) {
    std::list<string_type> sl;
    if((_info->range.num_encoding_sizes > 0) &&
       (_info->range.num_encoding_sizes < IW_MAX_ENCODING_SIZES)) {
      // Save the strings 
      for(int k = 1; k < _info->range.num_encoding_sizes; k++) {
	std::stringstream s;
	s << _info->range.encoding_size[k] * 8 << " bits";
	sl.push_back(s.str());
      }
    }
    // Clear
    _key_model->clear();
    
    unsigned char key[IW_ENCODING_TOKEN_MAX];
    char buf[128];
    int cur_index = 0;
    // k == 0 is to get the current active key
    _req.u.data.pointer = caddr_t(key);
    _req.u.data.length  = IW_ENCODING_TOKEN_MAX;
    _req.u.data.flags   = 0;

    for(int k = 0; k <= _info->range.max_encoding_tokens; k++) {
      _req.u.data.pointer       = caddr_t(key);
      _req.u.data.length        = IW_ENCODING_TOKEN_MAX;
      _req.u.data.flags         = k;
      Gtk::TreeModel::Row row   = *(_key_model->append());
      row[_key_columns._number] = k;
      if(get_value(SIOCGIWENCODE, "key value") < 0) {
	row[_key_columns._value]  = "n/a";
	continue;
      }
      if((_req.u.data.flags & IW_ENCODE_DISABLED)||(_req.u.data.length == 0))
	row[_key_columns._value]  = "off";
      else {
	//  Display the key
	iw_print_key(buf,sizeof(buf),key,_req.u.data.length,_req.u.data.flags);
	row[_key_columns._value]  = buf;
      }
      if (k == cur_index) {
	_key_selection->select(row);
	if (_req.u.data.flags & IW_ENCODE_RESTRICTED)
	  _enc_mode->get_entry()->set_text("restricted");
	if(_req.u.data.flags & IW_ENCODE_OPEN)
	  _enc_mode->get_entry()->set_text("open");
	if (row[_key_columns._value] == "off" || 
	    row[_key_columns._value] == "n/a") {
	  _enc_on->set_active(false);
	}
      }
      // row[_key_columns._bits]  = _req.u.data.length * 8;
    }
  }
  else {
    _enc_on->set_sensitive(false);
    _enc_on->set_active(false);
  }
  bool on = _enc_on->get_active();
  _key_view->set_sensitive(on);
  _enc_mode->set_sensitive(on);
}

//____________________________________________________________________
void interface_dialog::on_enc_on_toggled()
{
  debug_guard g("interface_dialog::on_enc_on_toggled");
  if (!_init) return;
  bool on = _enc_on->get_active();
  _key_view->set_sensitive(on);
  _enc_mode->set_sensitive(on);
}

//____________________________________________________________________
void interface_dialog::on_enc_mode_changed()
{
  debug_guard g("interface_dialog::on_enc_mode_changed");
  if (!_init) return;
  string_type text = _enc_mode->get_entry()->get_text();
  if (text.empty()) return;
  if (text == "open")        _req.u.data.flags = IW_ENCODE_OPEN;
  if (text == "restricted")  _req.u.data.flags = IW_ENCODE_RESTRICTED;
  set_value(SIOCGIWENCODE, "Encryption mode");
}


//====================================================================
void interface_dialog::set_values(const string_type& name)
{
  debug_guard("set_values(%s)", name.c_str());
  _name = name;
  _info = new wireless_info;
  // Get basic information 
  if (iw_get_basic_config(_socket, name.c_str(), &(_info->b)) < 0) return;
  // Get range 
  if (iw_get_range_info(_socket, name.c_str(), &(_info->range)) >= 0) 
    _info->has_range = 1;
  _init = false;
  // Get AP address 
  if(iw_get_ext(_socket, name.c_str(), SIOCGIWAP, &_req) >= 0) {
    _info->has_ap_addr = 1;
    memcpy(&(_info->ap_addr), &(_req.u.ap_addr), sizeof (sockaddr));
  }

  std::stringstream s;
  s << _info->b.name << ": " << _name;
  _config_name->set_text(s.str());
  update_nwid();
  update_mode();
  update_freq();
  update_sens();
  update_nick();
  update_rate();
  update_rts();
  update_frag();
  update_power();
  update_retry();
  update_txpower();
  update_enc();
  _apply_button->set_sensitive(false);
  _init = true;
}

//____________________________________________________________________
void interface_dialog::error(const char* format, ...) 
{
  va_list ap;
  va_start(ap, format);
  static char buffer[1024];
  vsnprintf(buffer, 1024, format, ap);
  va_end(ap);
  Gtk::MessageDialog dia(buffer, false, Gtk::MESSAGE_ERROR);
  dia.run();
}

    
//____________________________________________________________________
void interface_dialog::set_value(int request, const char* rname)
{
  debug_guard g("set_value(%X, %s)", request, rname);
  return;
  if (iw_set_ext(_socket, _name.c_str(), request, &_req) < 0) 
    error("Error for wireless request \"SET %s\" on device %-1.16s:\n%s", 
	  rname, _name.c_str(), strerror(errno));
}


//____________________________________________________________________
bool interface_dialog::get_value(int request, const char* rname)
{
  debug_guard g("get_value(%X, %s)", request, rname);
  bool ret = (iw_get_ext(_socket, _name.c_str(), request, &_req) >= 0);
  if (!ret) 
    g.message("\"%s\" (%X) on %s: %s", rname, request, _name.c_str(), 
	      strerror(errno));
  return ret;
}

//____________________________________________________________________
void interface_dialog::changed() 
{
  _apply_button->set_sensitive(true);
}

//____________________________________________________________________
//
//  EOF
//

