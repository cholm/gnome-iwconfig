// generated 2005/11/5 2:52:54 CET by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to essid_dialog.hh_new

// you might replace
//    class foo : public foo_glade { ... };
// by
//    typedef foo_glade foo;
// if you didn't make any modifications to the widget

#ifndef _ESSID_DIALOG_HH
#  include "essid_dialog_glade.hh"
#  define _ESSID_DIALOG_HH
#  ifndef _ESSID_INFO_H
#    include <essid_info.hh>
#  endif

class essid_dialog : public essid_dialog_glade
{  
public:
  typedef Glib::ustring string_type;
  essid_dialog(int socket) : _socket(socket) {}
  void on_close_button_clicked();
  void on_value_en_group_changed();
  void set_values(string_type& ifn, essid_info& essid);
protected:
  string_type _ifname;
  essid_info* _essid;
  int         _socket;
};
#endif
//
// EOF
//
