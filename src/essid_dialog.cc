// generated 2005/11/5 2:52:54 CET by cholm@cholm.nbi.dk.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to essid_dialog.cc_new

// This file is for your program, I won't touch it again!

#include "config.h"
#include "util.hh"
#include "essid_dialog.hh"
#include <sstream>
#include <iwlib.h>

void essid_dialog::on_close_button_clicked()
{  
  debug_guard g("on_close_button_clicked");
  if (_index_en->get_active()) {
    _essid->_key_index = _index_spin->get_value_as_int();
    _essid->_key_value = "";
  }
  else {
    _essid->_key_index = 0;
    _essid->_key_value = _value_entry->get_text();
  }
  string_type::size_type space = _essid->_key_value.find_last_of(" \t");
  if (space != string_type::npos) 
    _essid->_key_value.erase(space, _essid->_key_value.size()-space);
  this->hide();
}

void essid_dialog::on_value_en_group_changed()
{  
  debug_guard g("on_value_en_group_changed");
  _value_entry->set_sensitive(_value_en->get_active());
  _index_spin->set_sensitive(_index_en->get_active());
}

void essid_dialog::set_values(string_type& ifn, essid_info& essid)
{  
  debug_guard g("set_values");
  _ifname      = ifn;
  _essid       = &essid;
  _value_entry->set_text(essid._key_value);
  _index_spin->set_value(essid._key_index);
  std::stringstream s;
  s << essid._essid << " [" << essid._addr << "]";
  _dialog_title->set_text(s.str());

  struct iw_range range;
  if(iw_get_range_info(_socket, _ifname.c_str(), &range) >= 0)
    _index_spin->set_range(1, range.max_encoding_tokens);
}

//
// EOF
//
