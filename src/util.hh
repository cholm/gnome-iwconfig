#ifndef UTIL_HH
#  define UTIL_HH
#  ifdef HAVE_CONFIG_H
#    include <config.h>
#  endif
#  ifndef __STRING__
#    include <string>
#  endif
#  ifndef __IOSTREAM__
#    include <iostream>
#  endif
#  ifndef __CSTDARG__
#    include <cstdarg>
#  endif

struct debug_guard
{
  std::string _msg;
  debug_guard(const char* format, ...) 
  {
    va_list ap;
    va_start(ap, format);
    static char buf[1024];
    vsnprintf(buf, 1024, format, ap);
    _msg = buf;
    va_end(ap);
    if (_debug) std::cout << "==> " << _msg << std::endl;
  }
  ~debug_guard() 
  {
    if (_debug) 
      std::cout << "<== " << _msg << std::endl;
  }
  void message(const char* format, ...) 
  {
    va_list ap;
    va_start(ap, format);
    static char buf[1024];
    vsnprintf(buf, 1024, format, ap);
    va_end(ap);
    if (_debug) std::cout << "=== " << buf << std::endl;
  }
  static bool _debug;
};

#endif
//
// EOF
//


      
      
