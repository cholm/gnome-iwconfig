#!/bin/sh
# generated 2005/10/31 23:19:24 CET by cholm@cholm.nbi.dk.(none)
# using glademm V2.6.0

if test "x$1" = "xclean" ; then 
        if test -f Makefile ; then make maintainer-clean ; fi
        find . -name Makefile           | xargs rm -f
        find . -name Makefile.in        | xargs rm -f
        find . -name "*~"               | xargs rm -f
        find . -name core               | xargs rm -f
        find . -name .libs              | xargs rm -rf
        find . -name .deps              | xargs rm -rf
        find . -name "*.lo"             | xargs rm -f
        find . -name "*.o"              | xargs rm -f
        find . -name "*.la"             | xargs rm -rf
        find . -name "*.log"            | xargs rm -rf
        find . -name "semantic.cache"   | xargs rm -rf
        find . -name "*Dict.*"          | xargs rm -rf
	find . -name "*_glade.hh"	| xargs rm -rf
	find . -name "*_glade.cc"	| xargs rm -rf

        rm -f   config/missing          \
                config/mkinstalldirs    \
                config/ltmain.sh        \
                config/config.guess     \
                config/config.sub       \
                config/install-sh       \
                config/ltconfig         \
                config/config.hh        \
                config/config.hh.in     \
                config/stamp-h          \
                config/stamp-h.in       \
                config/depcomp          \
                config/stamp-h1		\
		config/config.h.in	\
		config/config.rpath	

        rm -rf  aclocal.m4              \
                autom4te.cache          \
                config.cache            \
                config.status           \
                config.log              \
                configure               \
                INSTALL                 \
                ChangeLog               \
                configure-stamp         \
                build-stamp             \
                libtool                 \
                libtool.m4              \
                ltoptions.m4            \
                ltsugar.m4              \
                ltversion.m4            \
                *.root                  \
                .rootrc			\
		ABOUT-NLS		\
		install-sh		\
		m4		
	        
	rm -rf  po/Makefile.in		\
		po/Makefile.in.in	\
		po/Makevars		\
	        po/Makevars.template	\
	        po/Makevars.template	\
		po/POTFILES		\
		po/Rules-quot		\
		po/boldquot.sed		\
		po/en@boldquot.header	\
		po/en@quot.header	\
		po/insert-header.sin	\
		po/quot.sed		\
		po/remove-potcdate.sin	
		

        rm -rf  doc/html                        \
                doc/xml                         \
                doc/latex                       \
                doc/man                         \
                doc/doxyconfig                  \
                doc/header.html                 \
                doc/gnome-iwconfig.tags

        rm -rf  gnome-iwconfig-*.tar.gz
	exit 0
fi

if test ! -f install-sh ; then touch install-sh ; fi

MAKE=`which gnumake`
if test ! -x "$MAKE" ; then MAKE=`which gmake` ; fi
if test ! -x "$MAKE" ; then MAKE=`which make` ; fi
HAVE_GNU_MAKE=`$MAKE --version|grep -c "Free Software Foundation"`

if test "$HAVE_GNU_MAKE" != "1"; then 
echo Only non-GNU make found: $MAKE
else
echo `$MAKE --version | head -1` found
fi

if which autoconf2.50 >/dev/null 2>&1
then AC_POSTFIX=2.50
elif which autoconf >/dev/null 2>&1
then AC_POSTFIX=""
else 
  echo 'you need autoconfig (2.58+ recommended) to generate the Makefile'
  exit 1
fi
echo `autoconf$AC_POSTFIX --version | head -1` found

if which automake-1.9 >/dev/null 2>&1
then AM_POSTFIX=-1.9
elif which automake-1.8 >/dev/null 2>&1
then AM_POSTFIX=-1.8
elif which automake-1.7 >/dev/null 2>&1
then AM_POSTFIX=-1.7
elif which automake-1.6 >/dev/null 2>&1
then AM_POSTFIX=-1.6
elif which automake >/dev/null 2>&1
then AM_POSTFIX=""
else
  echo 'you need automake (1.8.3+ recommended) to generate the Makefile'
  exit 1
fi
echo `automake$AM_POSTFIX --version | head -1` found

echo This script runs configure and make...
echo You did remember necessary arguments for configure, right?

# autoreconf$AC_POSTFIX -fim _might_ do the trick, too.
#  chose to your taste
aclocal$AM_POSTFIX
gettextize -f
if test ! -e po/Makevars
then cp po/Makevars.template po/Makevars
fi
if test ! -e po/LINGUAS
then touch po./sr/LINGUAS
fi
autoheader$AC_POSTFIX
automake$AM_POSTFIX --add-missing --copy --gnu
autoconf$AC_POSTFIX
if test "x$1" = "xsetup" ; then 
    exit 0
fi
./configure $* && $MAKE
